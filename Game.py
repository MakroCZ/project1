import tkinter as tk


import random

from Martingale import Martingale
from Dalembert import Dalembert
from Fibonacci import Fibonacci


class Game(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        self.strategies = []

        self.grid()
        self.create_widgets()

    def create_widgets(self):
        """
        Generate GUI
        """
        self.label_starting_money = tk.Label(self, text="Počáteční peníze")
        self.label_starting_money.grid(row=0, column=0)
        self.entry_starting_money = tk.Entry(self)
        self.entry_starting_money.grid(row=0, column=1)

        self.label_wanted_money = tk.Label(self, text="Požadované peníze")
        self.label_wanted_money.grid(row=1, column=0)
        self.entry_wanted_money = tk.Entry(self)
        self.entry_wanted_money.grid(row=1, column=1)

        self.label_base_bet = tk.Label(self, text="Základní sázka")
        self.label_base_bet.grid(row=2, column=0)
        self.entry_base_bet = tk.Entry(self)
        self.entry_base_bet.grid(row=2, column=1)
        
        self.label_casino_limit = tk.Label(self, text="Maximální sázka (násobek základní)")
        self.label_casino_limit.grid(row=3, column=0)
        self.entry_casino_limit = tk.Entry(self)
        self.entry_casino_limit.grid(row=3, column=1)

        self.label_repeat_number = tk.Label(self, text="Počet opakování")
        self.label_repeat_number.grid(row=4, column=0)
        self.entry_repeat_number = tk.Entry(self)
        self.entry_repeat_number.grid(row=4, column=1)
        
        self.button_start = tk.Button(self, text="Start game", command=self.start_game)
        self.button_start.grid(row=5, column=0, columnspan=2)

        self.testing_values()

    def testing_values(self):
        """
         Set testing values to form
        """
        self.entry_starting_money.insert(0, 100)
        self.entry_wanted_money.insert(0, 200)
        self.entry_base_bet.insert(0, 1)
        self.entry_casino_limit.insert(0, 1000000000)
        self.entry_repeat_number.insert(0, 100)

    def start_game(self):
        """
        Start game after clicking start button
        """
        starting_money = int(self.entry_starting_money.get())
        wanted_money = int(self.entry_wanted_money.get())
        base_bet = int(self.entry_base_bet.get())
        casino_limit = int(self.entry_casino_limit.get())
        repeat_number = int(self.entry_repeat_number.get())

        results = self.simulate_n_games(starting_money, wanted_money, base_bet, casino_limit, repeat_number)
        self.save_results(results)

    def simulate_n_games(self, starting_money, wanted_money, base_bet, casino_limit, repeat_number):
        """
        :return: List of one-game list results
        """
        results = []

        for i in range(repeat_number):
            results.append(self.simulate_one_game(starting_money, casino_limit, base_bet, wanted_money))

        return results

    def simulate_one_game(self, starting_money, casino_limit, base_bet, wanted_money):
        """
        :return: Statistics for all strategies in list
        """
        self.strategies = []
        self.strategies.append(Martingale(starting_money, casino_limit, base_bet, wanted_money))
        self.strategies.append(Dalembert(starting_money, casino_limit, base_bet, wanted_money))
        self.strategies.append(Fibonacci(starting_money, casino_limit, base_bet, wanted_money))

        self.game_progress(self.strategies)
        stats = []
        for strategy in self.strategies:
            stats.append(strategy.get_stats())
        return stats

    def game_progress(self, strategies):
        """
        Simulate game procces, if at least one strategy is still running
        :param strategies: List of strategies
        """
        running = False
        for strategy in strategies:
            running = running or strategy.running
        while running:
            win = self.next_number()
            running = False
            for strategy in strategies:
                if strategy.running:
                    strategy.update_state(win)
                    running = running or strategy.running

    def next_number(self):
        """
        Generate random number <0;36>
        :return: True if generated number is winnable
        """
        number = random.randint(0, 36)
        if number % 2 == 1:
            winning_number = True
        else:
            winning_number = False
        return winning_number

    def save_results(self, results):
        """
        Save results each strategy to separate file, file name is strategy name
        """
        strategy_count = len(self.strategies)
        for i in range(strategy_count):
            file_result = open(self.strategies[i].name + "_results.csv", "w")
            file_result.write("Výhra;Počet kol;Maximální sázka;Minimální peníze;Peníze;Maximální řada proher\n")
            for round_result in results:
                line = round_result[i]
                file_result.write(line + "\n")
            file_result.close()
        print("Done")

app = Game()
app.master.title("Ruleta")
app.mainloop()
