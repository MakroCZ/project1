from GameStrategy import GameStrategy


class Dalembert(GameStrategy):

    def __init__(self, starting_money, casino_limit, minimal_bet, wanted_money):
        GameStrategy.__init__(self, starting_money, casino_limit, minimal_bet, wanted_money, "Dalembert")
        self.actual_bet = minimal_bet

    def change_bet(self, winning_number):
        if winning_number:
            if self.actual_bet > self.minimal_bet:
                self.actual_bet -= self.minimal_bet
            else:
                self.actual_bet = self.minimal_bet
        else:
            if self.actual_bet + self.minimal_bet > self.minimal_bet * self.casino_limit:
                self.actual_bet = self.minimal_bet * self.casino_limit
            else:
                self.actual_bet += self.minimal_bet

