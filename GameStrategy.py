class GameStrategy:

    def __init__(self, starting_money, casino_limit, minimal_bet, wanted_money, strategy_name):
        self.money = starting_money
        self.casino_limit = casino_limit
        self.minimal_bet = minimal_bet
        self.wanted_money = wanted_money

        self.running = True
        self.name = strategy_name

        self.number_of_rounds = 0
        self.max_bet = 0
        self.min_money = starting_money
        self.loses_line = 0
        self.max_loses_line = 0

        self.actual_bet = self.minimal_bet

    def update_state(self, winning_number):
        """
        Update information about strategy according to generated number
        :param winning_number: True if generated number is winnable
        """
        if winning_number:
            self.money += self.actual_bet
            self.loses_line = 0
        else:
            self.money -= self.actual_bet
            self.loses_line += 1

        self.number_of_rounds += 1
        if self.actual_bet > self.max_bet:
            self.max_bet = self.actual_bet
        if self.min_money > self.money:
            self.min_money = self.money

        if self.loses_line > self.max_loses_line:
            self.max_loses_line = self.loses_line

        if self.money <= 0 or self.money >= self.wanted_money:
            self.running = False

        self.change_bet(winning_number)

    def change_bet(self, winning_number):
        """
        Change bet, implemented in strategies
        :param winning_number:
        """
        pass

    def get_stats(self):
        """
        :return: Statistic list
        """
        if self.money >= self.wanted_money:
            win = True
        else:
            win = False

        stats = str(win) + ";" + str(self.number_of_rounds) + ";" + str(self.max_bet) + ";" + str(self.min_money) + ";" + str(self.money) + ";" + str(self.max_loses_line)

        return stats