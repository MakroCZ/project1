from GameStrategy import GameStrategy


class Fibonacci(GameStrategy):

    def __init__(self, starting_money, casino_limit, minimal_bet, wanted_money):
        GameStrategy.__init__(self, starting_money, casino_limit, minimal_bet, wanted_money, "Fibonacci")
        self.actual_bet = minimal_bet
        self.fibonacci_list = [1, 1]
        self.index = 0

    def change_bet(self, winning_number):
        if winning_number:
            if self.index > 1:
                self.index -= 2
            self.actual_bet = self.minimal_bet * self.get_bet_multiplier(self.index)
        else:
            self.index += 1
            self.actual_bet = self.minimal_bet * self.get_bet_multiplier(self.index)
            if self.actual_bet > self.minimal_bet * self.casino_limit:
                self.actual_bet = self.minimal_bet * self.casino_limit

    def get_bet_multiplier(self, fibonacci_index):
        """
        Calculate bet multiplier
        :param fibonacci_index: Fibonacci number index
        :return: Bet multiplier based on Fibonacci numbers
        """
        if fibonacci_index + 1 > len(self.fibonacci_list):
            self.generate_fibonacci(fibonacci_index)
        if fibonacci_index > 0:
            return self.fibonacci_list[fibonacci_index] + self.fibonacci_list[fibonacci_index - 1]
        else:
            return self.fibonacci_list[fibonacci_index]

    def generate_fibonacci(self, requested_index):
        """
        Generate missing Fibonacci numbers
        """
        for i in range(len(self.fibonacci_list), requested_index + 1):
            self.fibonacci_list.append(self.fibonacci_list[i - 2] + self.fibonacci_list[i - 1])
